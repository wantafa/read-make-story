<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>RMS - Home</title>
  <link rel="icon" href="img/Fiticon.png" type="image/png">

  <!-- Bootstrap core CSS -->
  <link href="{{asset('template/startbootstrap-small-business-gh-pages/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="{{asset('template/startbootstrap-small-business-gh-pages/css/small-business.css')}}" rel="stylesheet">
  <link href="{{asset('css/modern-business.css')}}" rel="stylesheet">
  

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="index.php">Read Make Story</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="{{ route('list.cerita') }}">Read</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('login') }}">Make</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Genre
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
              @foreach ($genre as $item)
              <a class="dropdown-item" href="{{ route('list.cerita.genre', $item->id) }}">{{ $item->name }}</a>
              @endforeach
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              @if (Auth::user() )
              {{ Auth::user()->name }}
              @else
                  
              Masuk
              @endif
            </a>
              @guest
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
              <a class="dropdown-item" href="{{ route('login') }}">{{ __('Login') }}</a>
              @if (Route::has('register'))
              <a class="dropdown-item" href="{{ route('register') }}">{{ __('Daftar') }}</a>
              @endif
              @else
              {{-- {{ Auth::user()->name }} --}}
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
              <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="dropdown-item has-icon text-danger">{{ __('Logout') }}
                <i class="fas fa-sign-out-alt"></i></a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                  @csrf
              </form>
            </div>
            @endguest
  
          </li>
        </ul>
      </div>
    </div>
  </nav>

  
 <!-- /.navbar -->


<!-- Page Content -->
<header>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <!-- Slide One - Set the background image for this slide in the line below -->
        <div class="carousel-item active" style="background-image: url('img/rms1.png')">
          <div class="carousel-caption d-none d-md-block">
            <h3></h3>
            <p></p>
          </div>
        </div>
        <!-- Slide Two - Set the background image for this slide in the line below -->
        <div class="carousel-item" style="background-image: url('img/rms2.png')">
          <div class="carousel-caption d-none d-md-block">
            <h3></h3>
            <p></p>
          </div>
        </div>
        <!-- Slide Three - Set the background image for this slide in the line below -->
        <div class="carousel-item" style="background-image: url('img/rms3.png')">
          <div class="carousel-caption d-none d-md-block">
            <h3>Third Slide</h3>
            <p>This is a description for the third slide.</p>
          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </header>

    <!-- Page Content -->
    <div class="container">

        <h1 class="my-4">Welcome to RMS</h1>
    
        <!-- Marketing Icons Section -->
        <center>
        <div class="row">
          <div class="col-lg-4 mb-4">
            <div class="card h-100">
              <h4 class="card-header">Read</h4>
              <div class="card-body">
                <p class="card-text"><img src="img/read.jpg"></p>
              </div>
              <div class="card-footer">
                <a href="{{ route('list.cerita') }}" class="btn btn-primary">Mulai Membaca Cerita</a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 mb-4">
            <div class="card h-100">
              <h4 class="card-header">Write</h4>
              <div class="card-body">
                <p class="card-text"><img src="img/write.jpg"></p>
              </div>
              <div class="card-footer">
                <a href="{{ route('login') }}" class="btn btn-primary">Mulai Membuat Cerita</a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 mb-4">
            <div class="card h-100">
              <h4 class="card-header">Sign in or Sign Up</h4>
              <div class="card-body">
                <p class="card-text"><img src="img/akun.jpg"></p>
              </div>
              <div class="card-footer">
                <a href="{{ route('login') }}" class="btn btn-primary">Yes!</a>
              </div>
            </div>
          </div>
        </div>
    </center>
    </div>


<!-- Footer -->
  
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Kelompok 18 2021</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

</body>

</html>

<!-- /.footer -->