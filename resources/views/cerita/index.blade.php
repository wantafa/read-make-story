@extends ('layouts.master')
@section('title', 'Cerita')
@section('content')
<div class="content">
</div>

<div class="section-header">
    <h1>Cerita</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
      <div class="breadcrumb-item">Cerita</div>
    </div>
  </div>

    <section class="content" style="padding-top: 5px">
      <div style="margin-bottom: 10px;" class="row">
    <div class="col-lg-12">
          <button class="btn btn-icon icon-left btn-primary" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-plus"></i>Tambah Data</button>
            @if (session('status'))
                <div class="">
                    {{ session('status') }}
                </div>
            @endif
    </div>
      </div>

<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4>Cerita</h4>
        </div>
        <div class="col-md-3">
        <a href="{{ route('list.cerita') }}" class="btn btn-icon icon-left btn-sm btn-warning"><i class="fas fa-book"></i>Lihat Cerita</a>
      </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-hover table-striped" id="table-1">
              <thead>                                 
                <tr class="table-info">
                  <th>No</th>
                  <th>Judul</th>
                  <th>Genre</th>
                  <th>Konten</th>
                  <th>Poster</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>             
                @foreach ($cerita as $item)
                <tr>
                  <td>{{ $no++ }}</td>
                  <td>{{ $item->judul}}</td>
                  <td>{{ $item->genre->name}}</td>
                  <td>{{ $item->content}}</td>
                  <td><img width="90" height="90" src="{{ $item->poster}}"/></td>
                  <td>
                  
                  <a href="#" data-id="{{$item->id}}" class="btn btn-icon btn-success btn-sm btn-show"><i class="far fa-eye"></i></a>
                  <a href="/cerita/{{$item->id}}/edit" class="btn btn-icon btn-primary btn-sm btn-edit"><i class="far fa-edit"></i></a>
                  <a href="#" data-id="{{$item->id}}" class="btn btn-icon btn-danger btn-sm swal-6 "><i class="fas fa-trash"></i></a>
                  <form action="{{ route('cerita.delete', $item->id) }}" id="delete{{ $item->id }}" method="post" style="display: inline-block;">
                     @method('delete')
                     @csrf        
                    </form>
                    </td>
                  </tr>
                @endforeach                    
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
    </section>
  @section('modal')

  {{-- MODAL TAMBAH DATA --}}
  <div class="modal fade" tabindex="-1" role="dialog" id="exampleModal">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title">Tambah Cerita</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <form action="{{route('store')}}" method="POST" enctype="multipart/form-data">
                @csrf
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="judul" class="text">Judul</label>
                      <input id="judul" name="judul" placeholder="Masukkan Judul" type="text" class="form-control">
                      @error('judul')
                        <div class="alert alert-danger">
                          {{ $message }}
                        </div>
                      @enderror
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="genre_id" class="text">Genre Cerita</label>
                      <select name="genre_id" id="genre_id" class="form-control">
                        <option value=""> ---Pilih Genre-- </option>
                        @foreach ($genre as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                      </select>
                      @error('genre_id')
                        <div class="alert alert-danger">
                          {{ $message }}
                        </div>
                      @enderror
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="content" class="text">Content</label>
                      <textarea id="content" name="content" placeholder="Masukkan Content" type="text" class="form-control"></textarea>
                      @error('content')
                        <div class="alert alert-danger">
                          {{ $message }}
                        </div>
                    @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="poster" class="text">Poster</label>
                      <img width="200" height="200"/>
                      <input type="file" id="poster" name="poster" class="form-control border-primary uploads" style="margin-top: 20px;">
                      @error('poster')
                      <div class="alert alert-danger">
                        {{ $message }}
                      </div>
                    @enderror
                    </div>
                  </div>
            </div>
          </div>
              <div class="modal-footer bg-whitesmoke br">
                  <button type="button" class="btn btn-dark" data-dismiss="modal">Tutup</button>
                  <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
          </div>
      </div>
  </div>

  {{-- MODAL LIHAT DATA --}}
  <div class="modal fade" tabindex="-1" role="dialog" id="modal-lihat">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title">Lihat Cerita</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <form action="" method="POST" id="form-show">
                @csrf
              <div class="modal-body">
                
              </div>
              <div class="modal-footer bg-whitesmoke br">
                  <button type="button" class="btn btn-dark" data-dismiss="modal">Tutup</button>
                  {{-- <button type="submit" class="btn btn-primary">Simpan</button> --}}
              </div>
            </form>
          </div>
      </div>
  </div>

  {{-- MODAL EDIT DATA --}}
 
  @endsection

@endsection
@push ('page-scripts')
@include('cerita.js.cerita-js')
@endpush