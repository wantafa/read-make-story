@extends('cerita.sesudahlogin.master')
@section('title', 'Detail Cerita')
@section('content')

<style>
    blockquote {
            background: #f9f9f9;
            border-left: 10px solid #ccc;
            margin: 1.5em 10px;
            padding: 0.5em 10px;
            quotes: "\201C""\201D""\2018""\2019";
        }
        blockquote:before {
            color: #ccc;
            content: open-quote;
            font-size: 4em;
            line-height: 0.1em;
            margin-right: 0.25em;
            vertical-align: -0.4em;
        }
        blockquote p {
            text-align: start;
            font-style: italic;
            margin: 0 0 0 0;
        }
        blockquote h6 {
            text-align: start;
            font-weight: 700;
            padding: 0;
            margin: 0 0 .25rem;
        }
        .child-comment {
            padding-left: 50px;
        }
</style>

    <!-- Page Content -->
    <div class="container">

        <!-- Marketing Icons Section -->
        <center>
        <div class="row">
          <div class="col-md-12 mb-3 mt-5">
            <div class="card">
                <div class="card-body">
                <p class="card-text"><img src="../../{{$cerita->poster}}" width="200" height=""></p>
                  <h5 class="card-title text-left">{{ $cerita->judul }}</h5>
                  <h6 class="card-subtitle mb-2 text-muted text-left">{{ $cerita->genre->name }}</h6>
                  <p class="card-text text-justify">{{ $cerita->content }}</p>
                  <footer class="blockquote-footer">Posted on {{ ($cerita->created_at)->format('d F Y') }} by <cite title="Source Title">{{ $cerita->user->name }}</cite></footer>

                </div>
              </div>
          </div>
          
        </div>
    </center>
    </div>

    {{-- Komentar --}}
<center>
    <div class="col-md-6">
    <h5>Komentar</h5>
            <div class="row">
                <div class="col-md-6">
                    <form action="{{ url('/komentar') }}" method="post">
                        @csrf
                        <input type="hidden" name="id" value="{{ $cerita->id }}" class="form-control">
                        <input type="hidden" name="parent_id" id="parent_id" class="form-control">
                        <div class="form-group">
                            <label for="">Username</label>
                            <input type="text" class="form-control" name="username" value="{{ Auth::user()->username }}" readonly>
                            <p class="text-danger">{{ $errors->first('username') }}</p>
                        </div>
                        <div class="form-group" style="display: none" id="formReplyComment">
                            <label for="">Balas Komentar</label>
                            <input type="text" id="replyComment" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                            <label for="">Komentar</label>
                            <textarea name="komentar" cols="30" rows="10" class="form-control"></textarea>
                        </div>
                        <button class="btn btn-primary btn-sm mb-3">Kirim</button>
                    </form>
                </div>
                <div class="col-md-6">
                    @foreach ($cerita->komentar as $row)
                        <blockquote>
                            <h6>{{ $row->username ?? null }}</h6>
                            <hr>
                            <p>{{ $row->komentar ?? null }}</p><br>
                            <p><a class="text-left" onclick="balasKomentar({{ $row->id }}, '{{ $row->komentar }}'')" href="javascript:void(0)">Balas</a></p>
                        </blockquote>
                        @foreach ($row->komen as $val) 
                            <div class="child-comment">
                                <blockquote>
                                    <h6>{{ $val->username ?? null }}</h6>
                                    <hr>
                                    <p>{{ $val->komentar ?? null }}</p><br>
                                </blockquote>
                            </div>
                        @endforeach
                    @endforeach
                </div>
            </div>
        </div>
        </center>
@endsection
@push('script')
<script>
    function balasKomentar(id, content) {
        $('#formReplyComment').show();
        $('#parent_id').val(id)
        $('#replyComment').val(content)
    }
</script>
@endpush
