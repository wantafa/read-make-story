@extends('cerita.sesudahlogin.master')
@section('title', 'Cerita')
@section('content')


<header>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <!-- Slide One - Set the background image for this slide in the line below -->
        <div class="carousel-item active" style="background-image: url('img/rms1.png')">
          <div class="carousel-caption d-none d-md-block">
            <h3></h3>
            <p></p>
          </div>
        </div>
        <!-- Slide Two - Set the background image for this slide in the line below -->
        <div class="carousel-item" style="background-image: url('img/rms2.png')">
          <div class="carousel-caption d-none d-md-block">
            <h3></h3>
            <p></p>
          </div>
        </div>
        <!-- Slide Three - Set the background image for this slide in the line below -->
        <div class="carousel-item" style="background-image: url('img/rms3.png')">
          <div class="carousel-caption d-none d-md-block">
            <h3>Third Slide</h3>
            <p>This is a description for the third slide.</p>
          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </header>

    <!-- Page Content -->
    <div class="container">

        <h1 class="my-4">Welcome to RMS</h1>
    
        <!-- Marketing Icons Section -->
        <center>
        <div class="row">
          <div class="col-lg-4 mb-4">
            <div class="card h-100">
              <h4 class="card-header">Read</h4>
              <div class="card-body">
                <p class="card-text"><img src="img/read.jpg"></p>
              </div>
              <div class="card-footer">
                <a href="{{ route('list.cerita') }}" class="btn btn-primary">Mulai Membaca Cerita</a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 mb-4">
            <div class="card h-100">
              <h4 class="card-header">Write</h4>
              <div class="card-body">
                <p class="card-text"><img src="img/write.jpg"></p>
              </div>
              <div class="card-footer">
                <a href="{{ route('login') }}" class="btn btn-primary">Mulai Membuat Cerita</a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 mb-4">
            <div class="card h-100">
              <h4 class="card-header">Sign in or Sign Up</h4>
              <div class="card-body">
                <p class="card-text"><img src="img/akun.jpg"></p>
              </div>
              <div class="card-footer">
                <a href="{{ route('login') }}" class="btn btn-primary">Yes!</a>
              </div>
            </div>
          </div>
        </div>
    </center>
    </div>

@endsection
