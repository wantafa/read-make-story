<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Alert;
use App\Like;
use App\Genre;
use App\Cerita;
use App\Komentar;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use File;
use Illuminate\Support\Facades\Session;

class CeritaController extends Controller
{
    public function index() {
        $user_id=Auth::user()->id;
        $genre=DB::table('genre')->get();
        if(Auth::user()->role == 'user') {
        $cerita = Cerita::where('user_id', '=', $user_id)->get();
    }
        $cerita = Cerita::all();
        $no = 1;
        return view('cerita.index', compact('cerita', 'no','genre','user_id'));
    }

    public function create() {
        
        return view('cerita', compact('data','no'));
    }

    public function store(Request $request)
    {
        $genre=DB::table('genre')->get();
        $user_id=Auth::user()->id;
        $this->validate($request,[
            'judul' => 'required',
            'content' => 'required',
            'poster' => 'mimes:jpeg,jpg,png|max:2200',
            'genre_id' => 'required'
        ],
      [
          'judul.required' => 'Harus diisi',
          'content.required' => 'Harus diisi',
          'poster.required' => 'Harus dinput',
          'genre_id.required' => 'Harus diisi',
          
       ]);

        $data = $request->all();

        $poster = $request->poster;
        $new_poster = 'poster/' . time() . ' - ' .$poster->getClientOriginalName();

        Cerita::create([
            'judul' => $request->judul,
            'user_id' => $user_id,
            'content' => $request->content,
            'poster' => $new_poster,
            'genre_id' => $request->genre_id,
            
        ]);

        $poster->move('poster/', $new_poster);

        $cerita = Cerita::all();
        $no = 1;
        alert()->success('Berhasil','Data Cerita ditambahkan!');
        return view('cerita.index', compact('cerita', 'no','genre','user_id'));

    }

    public function show($id)
    {
        $cerita = Cerita::find($id);
        if(Auth::user()->role == 'user') {
            Alert::error('Maaf..', 'Anda dilarang masuk ke area ini.');
            return redirect()->to('/dashboard');
        }
        return view('cerita.show', compact('cerita'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function edit($id)
     {
        $cerita = Cerita::find($id);
        $genre = DB::table('genre')->get();
        $user_id=Auth::user()->id;
       return view('cerita.edit', compact('cerita','genre','user_id'));
     }
     
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $genre=DB::table('genre')->get();
        $user_id=Auth::user()->id;
        $this->validate($request,[
            'judul' => 'required',
            'content' => 'required',
            'poster' => 'mimes:jpeg,jpg,png|max:2200',
            'genre_id' => 'required'
        ],
      [
          'judul.required' => 'Harus diisi',
          'content.required' => 'Harus diisi',
          'genre_id.required' => 'Harus diisi',
          
       ]);
        $cerita = Cerita::findorfail($id);

        if($request->has('poster')){
            $path = "poster/";
            File::delete($path.$cerita->poster);
            $poster = $request->poster;
            $new_poster = 'poster/' . time() . ' - ' .$poster->getClientOriginalName();
            $poster->move('poster/', $new_poster);
            $cerita_data = [
                'judul' => $request->judul,
                'user_id' => $user_id,
                'content' => $request->content,
                'poster' => $new_poster,
                'genre_id' => $request->genre_id,
            ];
        } else{
            $cerita_data = [
                'judul' => $request->judul,
                'user_id' => $user_id,
                'content' => $request->content,
                'genre_id' => $request->genre_id,
            ];
        }
        
        $cerita->update($cerita_data);
        $no = 1;
        $cerita = Cerita::all();
        //return view('cerita.index', compact('cerita', 'no','genre','user_id'));
        return redirect()->action([CeritaController::class, 'index']);
       
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function destroy($id)
    {
        $cerita = Cerita::findOrFail($id);
        $cerita->delete();
        $path="poster/";
        File::delete($path . $cerita->poster);
        return redirect()->route('cerita');
    }

    public function show_cerita($id)
    {
        // $cerita = Cerita::find($id);
        $genre = Genre::all();
        $cerita = Cerita::with(['komentar', 'komentar.komen'])->where('id', $id)->first();
        return view('cerita.sesudahlogin.read', compact('cerita','genre'));
    }

    public function list_cerita()
    {
        $genre = Genre::all();
        $cerita = Cerita::all();
        return view('cerita.sesudahlogin.list_cerita', compact('cerita', 'genre'));
    }

    public function by_genre($id)
    {
        $genre = Genre::all();
        $genre_id = Genre::find($id)->id;
        $cerita = Cerita::where('genre_id', '=', $genre_id )->get();
        return view('cerita.sesudahlogin.list_cerita_by_genre', compact('cerita','genre'));
      
    }

    public function like(Request $request)
    {
      $cerita_id = $request['ceritaId'];
      $is_like = $request['isLike '] === 'true';
      $update = false;
      $cerita = Cerita::find($cerita_id);

      if (!$cerita) {
          return null;
      }

      $user = Auth::user();
      $like = $user->likes()->where('cerita_id', $cerita_id)->first();

      if ($like) {
          $already_like = $like->like;
          $update = true;
    
        if ($already_like == $is_like) {
            $like->delete();
            return null;
        }
      } else {
          $like = new Like();
      }
      $like->like = $is_like;
      $like->user_id = $user->id;
      $like->cerita_id = $cerita->id;

      if ($update) {
          $like->update();
      } else {
          $like->save();
      }
      return null;
    }
}
// push baru dari heroku